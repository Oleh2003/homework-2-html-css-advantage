"use strict"

// document.addEventListener("DOMContentLoaded", iconMenu)

// function iconMenu (event){
//     let target = event.target;

//     if(target.closest(".header__menu-icon")){
//         document.documentElement.classList.toggle('active');
//     }
    
// }

const menu = document.querySelector('.header__menu-icon');

const menuNavbar = document.querySelector('.menu__navbar');

menu.addEventListener('click', () => {
    menu.classList.toggle('active');
    menuNavbar.classList.toggle('active');
})

